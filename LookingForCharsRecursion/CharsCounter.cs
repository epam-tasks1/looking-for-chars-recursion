﻿using System;

namespace LookingForCharsRecursion
{
    public static class CharsCounter
    {
        public static int GetCharsCount(string str, char[] chars)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (chars is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (chars.Length == 0)
            {
                return 0;
            }

            if (str.Length == 0)
            {
                return 0;
            }

            if (str.Contains(chars[0], StringComparison.Ordinal))
            {
                return GetCharsCount(str, chars[1..]) + GetOneCharCount(str, chars[0]);
            }

            return GetCharsCount(str, chars[1..]);
        }

        public static int GetCharsCount(string str, char[] chars, int startIndex, int endIndex)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (chars is null)
            {
                throw new ArgumentNullException(nameof(chars));
            }

            if (chars.Length == 0)
            {
                return 0;
            }

            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (endIndex < 0 || endIndex > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(endIndex));
            }

            if (startIndex > str.Length || startIndex > endIndex)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            string trimmedStr = str[startIndex .. (endIndex + 1)];

            if (trimmedStr.Contains(chars[0], StringComparison.Ordinal))
            {
                return GetCharsCount(str, chars[1..], startIndex, endIndex) + GetOneCharCount(trimmedStr, chars[0]);
            }

            return GetCharsCount(str, chars[1..], startIndex, endIndex);
        }

        public static int GetCharsCount(string str, char[] chars, int startIndex, int endIndex, int limit)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (chars is null)
            {
                throw new ArgumentNullException(nameof(chars));
            }

            if (chars.Length == 0)
            {
                return 0;
            }

            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (endIndex < 0 || endIndex > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(endIndex));
            }

            if (startIndex > str.Length || startIndex > endIndex)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (limit < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(limit));
            }

            string trimmedStr = str[startIndex .. (endIndex + 1)];
            int subIndex = 0;

            if (trimmedStr.Contains(chars[0], StringComparison.Ordinal))
            {
                subIndex = GetOneCharCount(trimmedStr, chars[0]);
            }

            subIndex = GetCharsCount(str, chars[1..], startIndex, endIndex, limit) + subIndex;

            return (subIndex > limit) ? limit : subIndex;

            throw new NotImplementedException();
        }

        public static int GetOneCharCount(string str, char value)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (str.Length == 0)
            {
                return 0;
            }

            if (str[0] == value)
            {
                return GetOneCharCount(str[1..], value) + 1;
            }

            return GetOneCharCount(str[1..], value);
        }
    }
}
